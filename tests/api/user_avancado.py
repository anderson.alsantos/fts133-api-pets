import os
import pytest
import requests
import csv

# Variaves Fixas
url_api = 'https://petstore.swagger.io/v2'
headers = {'Content-Type': 'application/json'}
status_code_esperado = 200
caminho_absoluto_cadastrar = 'vendors/json/cadastrar_user.json'
caminho_absoluto_alterar = 'vendors/json/cadastrar_user2.json'
caminho_raiz = os.path.realpath('../..') +'/'
caminho_massa_dados_cadastrar='vendors/csv/cadastrar_user.csv'
caminho_massa_dados_consultar='vendors/csv/consultar_user.csv'
caminho_massa_dados_alterar='vendors/csv/alterar_user.csv'
caminho_massa_dados_deletar='vendors/csv/deletar_user.csv'


def ler_dados_csv(caminho):
    dados_csv = []

    try:
        with open(caminho) as arquivo_csv:
            campos = csv.reader(arquivo_csv, delimiter=';')
            next(campos)
            for linha in campos:
                dados_csv.append(linha)
        return dados_csv
    except FileNotFoundError:
        print(f'Arquivo não encontrado: {caminho}')
    except Exception as fail:
        print(f'Erro desconhecido: {fail}')


# Inicio dos testes
@pytest.mark.parametrize('id, cod_usuario, username, firstName, lastName, email, password, phone, userStatus, cod_status_esperado, type_esperado, msg_esperado, tipo_teste, obs', ler_dados_csv(caminho_raiz+caminho_massa_dados_cadastrar))
def test_cadastar_usuario(id, cod_usuario, username, firstName, lastName, email, password, phone, userStatus, cod_status_esperado, type_esperado, msg_esperado, tipo_teste, obs):
# Configura
    pload = [
        {"id": cod_usuario, "username": username, "firstName": firstName, "lastName": lastName, "email": email, "password": password, "phone": phone, "userStatus": userStatus}
    ]

# Execulta
    resultado_obtido = requests.post(url=url_api+'/user/createWithArray',
                                      json=pload,
                                      headers=headers
                                      )

# Valida
    if tipo_teste == 'positivo':
        assert resultado_obtido.status_code == float(status_code_esperado)
        assert resultado_obtido.json()['code'] == float(cod_status_esperado)
        assert resultado_obtido.json()['type'] == type_esperado
        assert resultado_obtido.json()['message'] == msg_esperado


@pytest.mark.parametrize('id, username,  cod_usuario_esperado,  username_esperado, email_esperado, cod_status_esperado, type_esperado, msg_esperado, tipo_teste, obs', ler_dados_csv(caminho_raiz+caminho_massa_dados_consultar))
def test_consultar_suaurio(id, username,  cod_usuario_esperado,  username_esperado, email_esperado, cod_status_esperado, type_esperado, msg_esperado, tipo_teste, obs):

# Configura

# Execulta
    resultado_obtido = requests.get(url=url_api + '/user/' + username,
                                     headers=headers)
# Valida
    if tipo_teste == 'positivo':
        assert resultado_obtido.status_code == float(cod_status_esperado)
        assert resultado_obtido.json()['id'] == float(cod_usuario_esperado)
        assert resultado_obtido.json()['username'] == username_esperado
        assert resultado_obtido.json()['email'] == email_esperado
    else:
        assert resultado_obtido.status_code == float(cod_status_esperado)
        assert resultado_obtido.json()['type'] == type_esperado
        assert  resultado_obtido.json()['message'] == msg_esperado


@pytest.mark.parametrize('id, cod_usuario, username, firstName, lastName, email, password, phone, userStatus, cod_status_esperado, type_esperado, msg_esperado, tipo_teste, obs', ler_dados_csv(caminho_raiz+caminho_massa_dados_alterar))
def teste_alterar_usuario(id, cod_usuario, username, firstName, lastName, email, password, phone, userStatus, cod_status_esperado, type_esperado, msg_esperado, tipo_teste, obs):
# Configura

    pload = {"id": cod_usuario, "username": username, "firstName": firstName, "lastName": lastName, "email": email, "password": password, "phone": phone, "userStatus": userStatus}

# Execulta
    resultado_obtido = requests.put(url=url_api + '/user/' + username,
                                      json=pload,
                                      headers=headers
                                    )
# Valida
    if tipo_teste == 'positivo':
        assert resultado_obtido.status_code == float(status_code_esperado)
        assert resultado_obtido.json()['code'] == float(cod_status_esperado)
        assert resultado_obtido.json()['type'] == type_esperado
        assert resultado_obtido.json()['message'] == cod_usuario

@pytest.mark.parametrize('id, username,  cod_usuario_esperado,  username_esperado, cod_status_esperado, type_esperado, msg_esperado, tipo_teste, obs', ler_dados_csv(caminho_raiz+caminho_massa_dados_deletar))
def test_deletar_usuario(id, username,  cod_usuario_esperado,  username_esperado,  cod_status_esperado, type_esperado, msg_esperado, tipo_teste, obs):
# Configura

# Execulta
    resultado_obtido = requests.delete(url=url_api + '/user/' + username,
                                       headers=headers)

# Valida
    if tipo_teste == 'positivo':
        assert resultado_obtido.status_code == float(status_code_esperado)
        assert resultado_obtido.json()['code'] == float(cod_status_esperado)
        assert resultado_obtido.json()['type'] == type_esperado
        assert resultado_obtido.json()['message'] == username
    else:
        assert resultado_obtido.status_code == float(cod_status_esperado)


