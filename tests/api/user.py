import os.path
import os
import pytest
import requests
import sys

# variaves fisicas
url_api = 'https://petstore.swagger.io/v2'
headers = {'Content-Type': 'application/json'}
status_code_esperado = 200
caminho_absoluto_cadastrar = 'vendors/json/cadastrar_user.json'
caminho_absoluto_alterar = 'vendors/json/cadastrar_user2.json'
caminho_raiz = os.path.realpath('../..') +'/'

def test_cadastar_usuario():
# Configura
    code_status = 200
    type_esperado = 'unknown'
    message_esperada = 'ok'
    caminho_json = caminho_raiz + caminho_absoluto_cadastrar

# Execulta
    resultado_obtido = requests.post(url=url_api+'/user/createWithArray',
                                      data=open(caminho_json, 'rb'),
                                      headers=headers
                                      )

# Valida
   # print(resultado_obtido.json())
    assert resultado_obtido.status_code == status_code_esperado
    assert resultado_obtido.json()['code'] == code_status
    assert resultado_obtido.json()['type'] == type_esperado
    assert resultado_obtido.json()['message'] == message_esperada


def test_consultar_suaurio():

# Configura
    nome_usuario = 'AndersonFTS133'
    id_esperado = 9999
    username_esperado = 'AndersonFTS133'
    email_esperado = "anderson@fts133.com.br"
# Execulta
    resultado_obtido = requests.get(url=url_api + '/user/' + nome_usuario,
                                     headers=headers)
# Valida
    assert resultado_obtido.status_code == status_code_esperado
    assert resultado_obtido.json()['id'] == id_esperado
    assert resultado_obtido.json()['username'] == username_esperado
    assert resultado_obtido.json()['email'] == email_esperado


def teste_alterar_usuario():
# Configura
    nome_usuario = 'AndersonFTS133'
    code_status = 200
    type_esperado = 'unknown'
    message_esperada = '9999'
    caminho_json = caminho_raiz + caminho_absoluto_alterar

# Execulta
    resultado_obtido = requests.put(url=url_api + '/user/' + nome_usuario,
                                      data=open(caminho_json, 'rb'),
                                      headers=headers
                                    )


# Valida

    assert resultado_obtido.status_code == status_code_esperado
    assert resultado_obtido.json()['code'] == code_status
    assert resultado_obtido.json()['type'] == type_esperado
    assert resultado_obtido.json()['message'] == message_esperada


def test_deletar_usuario():
# Configura
    nome_usuario = 'AndersonFTS133'
    code_status = 200
    type_esperado = 'unknown'
    message_esperada = nome_usuario
# Execulta
    resultado_obtido = requests.delete(url=url_api + '/user/' + nome_usuario,
                                       headers=headers)
    print(resultado_obtido)
# Valida
    assert resultado_obtido.status_code == status_code_esperado
    assert resultado_obtido.json()['code'] == code_status
    assert resultado_obtido.json()['type'] == type_esperado
    assert resultado_obtido.json()['message'] == message_esperada
